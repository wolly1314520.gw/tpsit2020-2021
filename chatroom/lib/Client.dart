import 'dart:io';

import 'package:chatroom/Message.dart';

class Client {
  Socket socket;
  String nome;
  static int n = 0;
  final List<Message> listMessage = [];

  Client() {
    n++;
    nome = 'client $n';
  }

  void connection() {
    Socket.connect("176.206.68.69", 3000).then((Socket socket) {
      socket = socket;
      socket.listen(dataHandler,
          onError: errorHandler, onDone: doneHandler, cancelOnError: false);
    }).catchError((Error e) {
      print("Connection failed: $e");
      exit(1);
    });
  }

  void dataHandler(data) {
    String mesData = String.fromCharCodes(data).trim();
    listMessage.add(Message(mesData, false));
  }

  void errorHandler(error, StackTrace trace) {
    print(error);
  }

  void doneHandler() {
    socket.destroy();
    exit(0);
  }

  void message(String s) {
    socket.write(this.nome + ' ' + s.trim() + '\n');
  }
}
