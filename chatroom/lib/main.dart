import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

import 'Client.dart';
import 'package:flutter/material.dart';

import 'Message.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return new MaterialApp(
      title: "Chatroom",
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: new MyHomePage(title: 'Chatroom'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Client c = Client();
  TextEditingController controller = new TextEditingController();

  _messages(Message message) {
    return Align(
      alignment: message.me ? Alignment.centerRight : Alignment.centerLeft,
      child: Container(
        margin: message.me
            ? EdgeInsets.only(
                top: 5.0,
                bottom: 5.0,
                left: 150.0,
              )
            : EdgeInsets.only(
                top: 5.0,
                bottom: 5.0,
              ),
        padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0),
        decoration: BoxDecoration(
          color: message.me ? Theme.of(context).accentColor : Color(00000),
          borderRadius: message.me
              ? BorderRadius.only(
                  topLeft: Radius.circular(15.0),
                  bottomLeft: Radius.circular(0.0),
                )
              : BorderRadius.only(
                  topRight: Radius.circular(15.0),
                  bottomRight: Radius.circular(0.0),
                ),
        ),
        child: Text(
          message.m,
          style: TextStyle(
            color: message.me ? Colors.white : Colors.black,
            fontSize: 16.0,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }

  _send() {
    setState(() {
      c.listMessage.add(Message(controller.text, true));
    });
    controller.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(widget.title),
          centerTitle: true,
          actions: <Widget>[
            RawMaterialButton(
              elevation: 2,
              fillColor: Colors.blue,
              child: Icon(
                Icons.wifi_rounded,
                size: 20.0,
                color: Colors.white,
              ),
              padding: EdgeInsets.all(10.0),
              shape: CircleBorder(),
              onPressed: () {
                c.connection();
              },
            ),
          ]),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            new Container(),
            Expanded(
                child: new ListView.builder(
                    itemCount: c.listMessage.length,
                    itemBuilder: (BuildContext ctxt, int Index) {
                      return _messages(c.listMessage[Index]);
                    })),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () {
                    setState(() {
                      c.listMessage.clear();
                    });
                  },
                  iconSize: 25,
                  color: Theme.of(context).primaryColor,
                ),
                Expanded(
                  child: TextField(
                      decoration: InputDecoration.collapsed(
                        hintText: 'send a message',
                      ),
                      textCapitalization: TextCapitalization.sentences,
                      controller: controller,
                      onSubmitted: (text) {
                        c.message(text);
                        //controller.clear();
                        _send();
                      }),
                ),
                IconButton(
                  icon: Icon(Icons.send),
                  iconSize: 25,
                  color: Theme.of(context).primaryColor,
                  onPressed: () => {
                    _send(),
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
