import 'package:flutter/material.dart';
import 'dart:async';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _hours = 0;
  int _minutes = 0;
  int _seconds = 0;
  String _timer = '00:00:00';

  Stream<int> _stream;
  StreamSubscription _streamSubscription;

  @override
  void initState() {
    super.initState();
    _stream = Stream.periodic(Duration(seconds: 1), (i) => i);
    _streamSubscription = _stream.listen((event) {
      if (_seconds == 59 && _minutes == 59) {
        setState(() {
          _minutes = 0;
          _seconds = 0;
          _hours++;
        });
      } else if (_seconds == 59) {
        setState(() {
          _seconds = 0;
          _minutes++;
        });
      } else {
        setState(() {
          _seconds++;
        });
      }
      setState(() {
        _timer = timer();
      });
    });
    _streamSubscription.pause();
  }

  String timer() {
    String hours = (_hours % 60).toString().padLeft(2, '0');
    String minutes = (_minutes % 60).toString().padLeft(2, '0');
    String seconds = (_seconds % 60).toString().padLeft(2, '0');

    return "$hours : $minutes : $seconds";
  }

  void startWatch() {
    if (_streamSubscription.isPaused) {
      setState(() {
        _streamSubscription.resume();
      });
    }
  }

  void stopWatch() {
    if (!_streamSubscription.isPaused) {
      setState(() {
        _streamSubscription.pause();
      });
    }
  }

  void resetWatch() {
    if (_streamSubscription.isPaused || !_streamSubscription.isPaused) {
      setState(() {
        _hours = _minutes = _seconds = 0;
        _timer = '00:00:00';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          centerTitle: true,
          title: new Text('StopWatch'),
        ),
        body: new Container(
            padding: EdgeInsets.all(80.0),
            child: new Column(
              children: <Widget>[
                new Text(_timer, style: new TextStyle(fontSize: 60.0)),
                SizedBox(height: 40.0),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new FloatingActionButton(
                        backgroundColor: Colors.green,
                        onPressed: startWatch,
                        child: new Icon(Icons.play_arrow)),
                    SizedBox(width: 40.0),
                    new FloatingActionButton(
                        backgroundColor: Colors.red,
                        onPressed: stopWatch,
                        child: new Icon(Icons.stop)),
                    SizedBox(width: 40.0),
                    new FloatingActionButton(
                        backgroundColor: Colors.blue,
                        onPressed: resetWatch,
                        child: new Icon(Icons.refresh)),
                  ],
                )
              ],
            )));
  }
}
